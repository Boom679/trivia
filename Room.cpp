#include "Room.h"


Room::Room(int id, User* ad, string name, int max, int qusetNo, int time)// init members
{
	_id = id;
	_admin = ad;
	_users.push_back(_admin);

	_admin = ad;
	_name = name;
	_questionTime = time;
	_questionNo = qusetNo;
	_maxUsers = max;
}

bool Room::JoinRoom(User* us) // try to join room
{
	if (_users.size() == _maxUsers) // if the room is full 
	{
		Helper::sendData(us->getSocket(), "1101"); // "room is full" message
		return false;
	}

	_users.push_back(us); 
	Helper::sendData(us->getSocket(), "1100" + Helper::getPaddedNumber(_questionNo, 2) + Helper::getPaddedNumber(_questionTime, 2));

	sendMessage(getUsersListMessage()); // send to all
	return true;
}

int Room::closeRoom(User* us) // check if the admin asking, and if so, close all the room members
{
	if (_admin->getUsername() != us->getUsername()) // check if the admin want to close
		return -1;

	for (unsigned int i = 0; i < _users.size(); i++) // send to all close room message
	{
		_users[i]->send("116");
		if (_admin->getUsername() != _users[i]->getUsername())
			_users[i]->clearRoom();
	}
	return _id;
}

void Room::leaveRoom(User* us) //leaving room
{
	for (unsigned int i = 0; i < this->_users.size(); i++) // finding the user that wants to leave
	{
		if (this->_users[i]->getUsername() == us->getUsername())
		{
			this->_users.erase(this->_users.begin() + i); // erase him
			Helper::sendData(us->getSocket(), "1120"); // leaving
			break;
		}
	}
	this->sendMessage(this->getUsersListMessage());
}

void Room::sendMessage(std::string str) // send message to all users
{
	for (unsigned int i = 0; i < _users.size(); i++)
	{
		_users[i]->send(str);
	}
}

void Room::sendMessage(User* us, std::string str) //send message to all users, but not to 'us' 
{
	for (unsigned int i = 0; i < _users.size(); i++)
	{
		if (_users[i]->getUsername() != us->getUsername())
			_users[i]->send(str);
	}
}

string Room::getUsersListMessage() // return the list of the users
{
	string message = "108";
	message = message + Helper::getPaddedNumber(_users.size(), 1);
	for (unsigned int i = 0; i < _users.size(); i++)
	{
		message = message + Helper::getPaddedNumber(_users[i]->getUsername().size(), 2);
		message = message + _users[i]->getUsername();
	}

	return message;
}

vector<User*> Room::getUsers()
{
	return _users;
}

int Room::getQuestionsNo()
{
	return _questionNo;
}

int Room::getId()
{
	return _id;
}

string Room::getName()
{
	return _name;
}