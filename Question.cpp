#include "Question.h"

Question::Question(int i, string question, string a1, string a2, string a3, string a4) // shuffle the answers options
{
	_id = i;
	_question = question;

	string correctAnswer = a1;

	vector<string> answers{ a1, a2, a3, a4 };
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

	shuffle(answers.begin(), answers.end(), std::default_random_engine(seed));

	for (int i = 0; i < 4; i++)
	{
		_answers[i] = answers[i];
		if (_answers[i] == correctAnswer)
			_correctAnswerIndex = i;
	}

}

string Question::getQuestion()
{
	return _question;
}

string* Question::getAnswers()
{
	return _answers;
}

int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}

int Question::getId()
{
	return _id;
}
