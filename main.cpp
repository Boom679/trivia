#include "TriviaServer.h"

int main()
{
	try
	{

		TRACE("Starting...");
		// NOTICE at the end of this block the WSA will be closed 
		WSADATA wsa_data = {};
		if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
			throw std::exception("WSAStartup Failed");
		TriviaServer* md_server = new TriviaServer();
		md_server->serve();
		WSACleanup();
		delete md_server;
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function: " << e.what() << std::endl;
		system("pause");
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
		system("pause");
	}
	
	return 0;
}