#pragma once
#include <iostream>
#include <sys/types.h>
#include <stdlib.h>
#include <sstream>
#include "Game.h"
#include "Helper.h"
#include "Room.h"


class Game;
class Room;

class User
{
	private:
		std::string _username;
		Room* _currRoom;
		Game* _currGame;
		SOCKET _sock;

	public:
		User(std::string s1, SOCKET s);//
		void send(std::string);//
		std::string getUsername();//
		SOCKET getSocket();//
		Room* getRoom();//
		Game* getGame();//
		void setGame(Game* g);//
		void clearRoom(); // 
		bool createRoom(int, std::string, int, int, int);//
		bool joinRoom(Room* r);//
		void leaveRoom();
		int closeRoom();
		bool leaveGame();

};