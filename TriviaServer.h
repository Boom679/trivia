#pragma once
#pragma comment(lib, "Ws2_32.lib")

#include <sstream>
#include <map>
#include <mutex>
#include <queue>
#include "RecievedMessage.h"
#include "User.h"
#include <exception>
#include <thread>
#include <condition_variable>
#include "Helper.h"
#include "Vaildator.h"
#include "DataBase.h"

class TriviaServer
{
	private:
		SOCKET _socket;
		std::condition_variable cv; // 
		std::map<SOCKET, User*> _connectedUsers;
		DataBase _db;
		std::map<int, Room*> _roomsList;
		std::mutex _mtxRecievedMessages;
		std::queue<RecievedMessage*> _queRcvMessages;
		static int _roomidSequence;

		void bindAndListen();
		void accept();
		void clientHandler(SOCKET sc);
		void safeDeleteUser(RecievedMessage* rm);
		User* handleSignin(RecievedMessage* rm);
		bool handleSignUp(RecievedMessage* rm);
		void handleSignOut(RecievedMessage* rm);
		////
		void handleLeaveGame(RecievedMessage* rm);
		void handleStartGame(RecievedMessage* rm);
		void handlePlayerAnswer(RecievedMessage* rm);

		bool handleCreateRoom(RecievedMessage* rm);
		bool handleCloseRoom(RecievedMessage* rm);
		bool handleJoinRoom(RecievedMessage* rm);
		bool handleLeaveRoom(RecievedMessage* rm);
		void handleGetUsersInRoom(RecievedMessage* rm);
		void handleGetRooms(RecievedMessage* rm);

		void handleGetBestScores(RecievedMessage* rm);
		void handleGetPersonalStatus(RecievedMessage* rm);
	
		void handleRecievedMessages();
		void addRecievedMessage(RecievedMessage* rm);
		RecievedMessage* buildRecievedMessage(SOCKET sc, int i);

		User* getUserByName(std::string);
		User* getUsersBySocket(SOCKET sc);
		Room* getRoomByid(int i);

	public:
		void serve();
		TriviaServer();
		~TriviaServer();

};