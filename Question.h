#pragma once
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <WinSock2.h>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm> 
#include <random>     
#include <chrono>

using namespace std;

class Question
{
	private:
		std::string _question;
		std::string _answers[4];
		int _correctAnswerIndex;
		int _id;

	public:
		Question(int i, string question, string a1, string a2, string a3, string a4);
		std::string getQuestion();
		std::string* getAnswers();
		int getCorrectAnswerIndex();
		int getId();
};