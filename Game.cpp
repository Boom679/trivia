#include "Game.h"

Game::Game(const std::vector<User*>& players, int questionsNo, DataBase& db) : _db(db)
{ 
	try
	{
		_game_id = _db.insertNewGame();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function: " << e.what() << std::endl;
		throw exception("prob");
		system("pause");
	}
	_questions = _db.initQuestions(questionsNo); // take random questions
	_questions_no = questionsNo;
	_currQuestionindex = 0;
	for (unsigned int i = 0; i < players.size(); i++)
	{
		_players.push_back(players[i]);
	}

	for (unsigned int i = 0; i < players.size(); i++)
	{
		_results.insert(pair<string, int>(players[i]->getUsername(), 0));
	}

	for (unsigned int i = 0; i < players.size(); i++)
	{
		_players[i]->setGame(this);
	}
	_currentTurnAnswers = 0;
}

Game::~Game()
{
	for (unsigned int i = 0; i < _questions.size(); i++)
	{
		delete _questions[i];
		_questions.erase(_questions.begin() + i);
	}
	for (unsigned int i = 0; i < _players.size(); i++)
	{
		delete _players[i];
		_players.erase(_players.begin() + i);
	}
}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

void Game::handleFinishGame()
{
	_db.updateGameStatus(_game_id); // update the game (finish)
	
	string message = "121" + to_string(_players.size()); // build a "end game" message
	for (unsigned int i = 0; i < _players.size(); i++)
	{
		message = message + Helper::getPaddedNumber(_players[i]->getUsername().size(), 2);
		message = message + _players[i]->getUsername();
		auto it = _results.find(_players[i]->getUsername());
		message = message + Helper::getPaddedNumber(it->second, 2);
	}
	for (unsigned int i = 0; i < _players.size(); i++) // send to all 
	{
		_players[i]->send(message);
	}
}

bool Game::handleNextTurn()
{
	if (_players.size() == 0) // 
	{
		handleFinishGame();
		return false;
	}

	if (_currentTurnAnswers == _players.size()) // if all the users has been answer
	{
		if (_currQuestionindex == _questions.size() - 1) // if the game over
		{
			handleFinishGame();
			return false;
		}
		_currQuestionindex++;
		sendQuestionToAllUsers();
		return true;
	}
	return true;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	bool flag = false, flag2 = false;
	_currentTurnAnswers++;
	if (answerNo == _questions[_currQuestionindex]->getCorrectAnswerIndex()) // if that is the right question
	{
		auto it = _results.find(user->getUsername());
		it->second++; // inc the score
		flag = true;
	}

	if (answerNo == 5) // player did not answer in time
	{
		_db.addAnswerToPlayer(_game_id, user->getUsername(), _questions[_currQuestionindex]->getId(), " ", flag, time);
		flag2 = true;
	}

	if (flag2 == false)
		_db.addAnswerToPlayer(_game_id, user->getUsername(), _questions[_currQuestionindex]->getId(), _questions[_currQuestionindex]->getAnswers()[answerNo], flag, time);

	if (flag == true) // correct
		user->send("1201");
	else
		user->send("1200");

	if (handleNextTurn() == true)
		return true;
	else
		return false;

}

bool Game::leaveGame(User* u)
{
	for (unsigned int i = 0; i < _players.size(); i++) // finding the user and erase him
	{
		if (u->getUsername() == _players[i]->getUsername())
		{
			_players.erase(_players.begin() + i);
			break;
		}
	}
	if (handleNextTurn() == true)
		return true;
	else
		return false;
}

int Game::getID()
{
	return _game_id;
}

void Game::sendQuestionToAllUsers() 
{
	string message = "118"; // build a question message
	message = message + Helper::getPaddedNumber(_questions[_currQuestionindex]->getQuestion().size(), 3);
	message = message + _questions[_currQuestionindex]->getQuestion();
	for (unsigned int i = 0; i < 4; i++)
	{
		message = message + Helper::getPaddedNumber(_questions[_currQuestionindex]->getAnswers()[i].size(), 3);
		message = message + _questions[_currQuestionindex]->getAnswers()[i];
	}
	_currentTurnAnswers = 0;
	for (unsigned int i = 0; i < _players.size(); i++) // send to all
	{
		_players[i]->send(message);
	}
}

